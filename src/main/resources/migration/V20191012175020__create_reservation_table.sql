CREATE TABLE `reservation`
(
    `id`                BIGINT PRIMARY KEY AUTO_INCREMENT,
    `username`          VARCHAR(128) NOT NULL,
    `company_name`      VARCHAR(64)  NOT NULL,
    `duration`          VARCHAR(10)  NOT NULL,
    `client_start_time` TIMESTAMP,
    `client_zone_id`    VARCHAR(64)  NOT NULL,
    `staff_id`          BIGINT       NOT NULL,
    FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
);
