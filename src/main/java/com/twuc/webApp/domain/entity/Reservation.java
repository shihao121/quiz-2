package com.twuc.webApp.domain.entity;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;

    @Column
    private String companyName;

    @Column
    private String duration;

    @Column
    private String clientZoneId;

    @Column
    private ZonedDateTime clientStartTime;

    @ManyToOne
    private Staff staff;

    public Reservation(String username, String companyName, String duration, String clientZoneId, ZonedDateTime clientStartTime) {
        this.username = username;
        this.companyName = companyName;
        this.duration = duration;
        this.clientZoneId = clientZoneId;
        this.clientStartTime = clientStartTime;
    }

    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    public String getClientZoneId() {
        return clientZoneId;
    }

    public ZonedDateTime getClientStartTime() {
        return clientStartTime;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
