package com.twuc.webApp.domain.web;

import com.twuc.webApp.domain.contract.*;
import com.twuc.webApp.domain.entity.Reservation;
import com.twuc.webApp.domain.entity.Staff;
import com.twuc.webApp.domain.repository.ReservationRepository;
import com.twuc.webApp.domain.repository.StaffRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

@RestController
@RequestMapping(value = "/api/staffs")
public class StaffController {

    private final StaffRepository staffRepository;

    private final ReservationRepository reservationRepository;

    private Logger log = LoggerFactory.getLogger(StaffController.class);

    public StaffController(StaffRepository staffRepository, ReservationRepository reservationRepository) {
        this.staffRepository = staffRepository;
        this.reservationRepository = reservationRepository;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid RequestStaff requestStaff) {
        Staff savedStaff = staffRepository.save(new Staff(requestStaff.getFirstName(), requestStaff.getLastName()));
        URI uri = URI.create(String.format("/api/staffs/%d", savedStaff.getId()));
        return ResponseEntity.created(uri).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        Optional<Staff> fetchedStaffOptional = staffRepository.findById(id);
        if (fetchedStaffOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).
                    contentType(MediaType.APPLICATION_JSON).body(fetchedStaffOptional.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Staff>> getStaffs() {
        List<Staff> fetchedStaffs = staffRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).
                contentType(MediaType.APPLICATION_JSON).body(fetchedStaffs);
    }

    @PutMapping(value = "/{staffId}/timezone")
    public ResponseEntity updateZoneId(@RequestBody RequestZoneId requestZoneId, @PathVariable Long staffId) {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        Optional<Staff> fetchedStaffOptional = staffRepository.findById(staffId);
        if (!availableZoneIds.contains(requestZoneId.getZoneId()) || !fetchedStaffOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            Staff fetchedStaff = fetchedStaffOptional.get();
            fetchedStaff.setZoneId(requestZoneId.getZoneId());
            staffRepository.save(fetchedStaff);
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping(value = "/{staffId}/reservations")
    public ResponseEntity createReservation(@RequestBody @Valid RequestClientReservation clientReservation
            , @PathVariable Long staffId) {
        ZonedDateTime clientZonedDateTime = ZonedDateTime.ofInstant(
                clientReservation.getStartTime().toInstant(), ZoneId.of(clientReservation.getZoneId()));
        System.out.println(clientZonedDateTime + "==========");
        Pattern durationPattern = Pattern.compile("^PT\\dH$");
        Optional<Staff> fetchedStaffOptional = staffRepository.findById(staffId);
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        if (!availableZoneIds.contains(clientReservation.getZoneId())
                || !durationPattern.matcher(clientReservation.getDuration()).find()
                || !fetchedStaffOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Staff fetchedStaff = fetchedStaffOptional.get();
        Reservation reservation = new Reservation(clientReservation.getUsername(),
                clientReservation.getCompanyName(), clientReservation.getDuration(), clientReservation.getZoneId(),
                clientZonedDateTime);
        reservation.setStaff(fetchedStaff);
        reservationRepository.save(reservation);
        URI createdURI = URI.create(String.format("/api/staffs/%d/reservations", staffId));
        return ResponseEntity.created(createdURI).build();
    }

    @GetMapping(value = "/{staffId}/reservations")
    public ResponseEntity getReservations(@PathVariable Long staffId) {
        Optional<Staff> fetchedStaffOptional = staffRepository.findById(staffId);
        if (!fetchedStaffOptional.isPresent() || fetchedStaffOptional.get().getZoneId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Staff fetchedStaff = fetchedStaffOptional.get();
        List<Reservation> reservations = reservationRepository.findByStaffId(staffId);
        List<ResponseReservation> responseReservations = new ArrayList<>();
        reservations.forEach(reservation -> {
            ResponseReservation responseReservation = new ResponseReservation(reservation.getUsername(), reservation.getCompanyName(), reservation.getDuration());
            responseReservation.setClientStartTime(reservation.getClientZoneId(), reservation.getClientStartTime().withZoneSameInstant(ZoneId.of(reservation.getClientZoneId())));
            ZonedDateTime staffStartTime = reservation.getClientStartTime().withZoneSameInstant(ZoneId.of(fetchedStaff.getZoneId()));
            responseReservation.setStaffStartTime(fetchedStaff.getZoneId(), staffStartTime);
            responseReservations.add(responseReservation);
        });
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                .body(responseReservations);
    }
}
