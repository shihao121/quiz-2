package com.twuc.webApp.domain.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/api/timezones")
public class TimeZoneController {

    @GetMapping
    public ResponseEntity<List<String>> get() {
        Stream<String> sortedAvailableZoneIds = ZoneRulesProvider.getAvailableZoneIds().stream().sorted();
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON).body(sortedAvailableZoneIds.collect(Collectors.toList()));
    }

}
