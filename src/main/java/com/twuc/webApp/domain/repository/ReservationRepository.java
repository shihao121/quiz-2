package com.twuc.webApp.domain.repository;

import com.twuc.webApp.domain.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByStaffId(Long id);

}
