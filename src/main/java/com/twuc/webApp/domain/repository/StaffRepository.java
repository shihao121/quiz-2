package com.twuc.webApp.domain.repository;

import com.twuc.webApp.domain.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepository extends JpaRepository<Staff, Long> {
}
