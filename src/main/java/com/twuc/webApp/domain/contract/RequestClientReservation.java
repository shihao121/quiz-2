package com.twuc.webApp.domain.contract;

import org.hibernate.validator.constraints.Length;

import java.time.ZonedDateTime;

public class RequestClientReservation {

    @Length(max = 128)
    private String username;

    @Length(max = 64)
    private String companyName;

    private String zoneId;

    private ZonedDateTime startTime;

    private String duration;

    public RequestClientReservation(@Length(max = 128) String username, @Length(max = 64) String companyName, String zoneId, ZonedDateTime startTime, String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public RequestClientReservation() {
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }
}
