package com.twuc.webApp.domain.contract;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class ResponseReservation{

    private String username;
    private String companyName;
    private String duration;
    private StartTime startTime = new StartTime();

    public ResponseReservation(String username, String companyName, String duration) {
        this.username = username;
        this.companyName = companyName;
        this.duration = duration;
    }

    public ResponseReservation() {
    }

    public void setClientStartTime(String zoneId, ZonedDateTime startTime) {
        this.getStartTime().getClient().setZoneId(zoneId);
        this.getStartTime().getClient().setStartTime(startTime);
    }

    public void setStaffStartTime(String zoneId, ZonedDateTime startTime) {
        this.getStartTime().getStaff().setStartTime(startTime);
        this.getStartTime().getStaff().setZoneId(zoneId);
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    private static class StartTime {
        public ZoneTime client = new ZoneTime();
        public ZoneTime staff = new ZoneTime();

        ZoneTime getClient() {
            return client;
        }

        ZoneTime getStaff() {
            return staff;
        }

        private static class ZoneTime {
            private String zoneId;
            private ZonedDateTime startTime;

            void setZoneId(String zoneId) {
                this.zoneId = zoneId;
            }

            void setStartTime(ZonedDateTime startTime) {
                this.startTime = startTime;
            }

            public String getZoneId() {
                return zoneId;
            }

            public ZonedDateTime getStartTime() {
                return startTime;
            }
        }

    }
}



