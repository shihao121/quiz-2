package com.twuc.webApp.domain.contract;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RequestStaff {

    public RequestStaff(@Size(max = 64) String firstName, @Size(max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public RequestStaff() {
    }

    @Length(max = 64)
    private String firstName;
    @NotNull

    @Length(max = 64)
    @NotNull
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
