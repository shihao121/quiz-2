package com.twuc.webApp.domain.contract;

public class ResponseStaff {

    private Long id;

    private String firstName;

    private String lastName;

    private String zoneId;

    public ResponseStaff(Long id, String firstName, String lastName, String zoneId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.zoneId = zoneId;
    }

    public ResponseStaff() {
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
