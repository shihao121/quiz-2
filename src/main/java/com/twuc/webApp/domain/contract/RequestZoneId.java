package com.twuc.webApp.domain.contract;

public class RequestZoneId {

    private String zoneId;

    public RequestZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public RequestZoneId() {
    }

    public String getZoneId() {
        return zoneId;
    }
}
