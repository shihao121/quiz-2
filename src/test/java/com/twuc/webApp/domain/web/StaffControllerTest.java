package com.twuc.webApp.domain.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.contract.RequestClientReservation;
import com.twuc.webApp.domain.contract.RequestStaff;
import com.twuc.webApp.domain.contract.RequestZoneId;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZonedDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {

    private static final String OVER_64_LENGTH_STRING = "qwertyuioplkjhhgsfasasadfadsfasdfadfadfasdfasffffffffffdafsdqeqrqdazadfadgadgadfadfadfqwedadfsdfadfadfadfadfad";
    private static final RequestStaff SAVED_STAFF = new RequestStaff("Rob", "hall");
    private static final int NOT_EXIST_STAFF_ID = 99;

    @Test
    void should_get_201_when_create_a_staff() throws Exception {
        String sendRequestStaff = serialize(new RequestStaff("Rob", "hall"));
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendRequestStaff))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void should_get_location_when_create_a_staff() throws Exception {
        String sendRequestStaff = serialize(new RequestStaff("Rob", "hall"));
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendRequestStaff)
        ).andExpect(MockMvcResultMatchers.header().string("location", "/api/staffs/1"));
    }

    @Test
    void should_get_400_when_i_given_a_incorrect_request_staff() throws Exception {
        String sendRequestStaff = serialize(new RequestStaff(
                OVER_64_LENGTH_STRING,
                "hall"));
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendRequestStaff)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_get_400_when_given_null() throws Exception {
        String sendRequestStaff = serialize(new RequestStaff(null, null));
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendRequestStaff)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_get_staff_info_when_i_staff_exist() throws Exception {
        String createdLocation = getLocation();
        String[] locationSplit = createdLocation.split("/");
        Integer staffId = Integer.parseInt(locationSplit[locationSplit.length - 1]);
        mockMvc.perform(get(createdLocation))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(staffId))
                .andExpect(jsonPath("$.firstName").value(SAVED_STAFF.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(SAVED_STAFF.getLastName()));
    }

    @Test
    void should_get_404_when_staff_is_not_exist() throws Exception {
        mockMvc.perform(get(String.format("api/staffs/%d", NOT_EXIST_STAFF_ID)))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_get_staffs_if_database_have_saved_staff() throws Exception {
        saveStaff();
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].firstName").value(SAVED_STAFF.getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(SAVED_STAFF.getLastName()));
    }

    @Test
    void should_return_empty_array_when_database_have_no_staff() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").doesNotExist());
    }

    @Test
    void should_return_400_when_zone_id_is_not_valid() throws Exception {
        Long savedStaffId = getSavedStaffId();
        mockMvc.perform(put(String.format("/api/staffs/%d/timezone", savedStaffId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new RequestZoneId("abc"))))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_zone_id_is_null() throws Exception {
        Long savedStaffId = getSavedStaffId();
        mockMvc.perform(put(String.format("/api/staffs/%d/timezone", savedStaffId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_update_staff_zone_id() throws Exception {
        Long savedStaffId = getSavedStaffId();
        mockMvc.perform(put(String.format("/api/staffs/%d/timezone", savedStaffId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new RequestZoneId("Asia/Chongqing"))))
                .andExpect(status().isOk());
        mockMvc.perform(get(String.format("/api/staffs/%d", savedStaffId)))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_return_null_when_zone_id_not_exist() throws Exception {
        Long savedStaffId = getSavedStaffId();
        mockMvc.perform(get(String.format("/api/staffs/%d", savedStaffId)))
                .andExpect(jsonPath("$.zoneId").doesNotExist());
    }

    @Test
    void should_get_zone_id_list() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists());
    }

    @Test
    void should_get_201_when_client_reservation() throws Exception {
        String requestClientReservation = serialize(new RequestClientReservation("Sofia",
                "ThoughtWorks", "Africa/Nairobi", ZonedDateTime.now(), "PT1H"));
        Long savedStaffId = getSavedStaffId();
        updateZoneId(savedStaffId, "Asia/Chongqing");
        mockMvc.perform(post(String.format("/api/staffs/%d/reservations", savedStaffId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestClientReservation))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "/api/staffs/1/reservations"));
    }

    @Test
    void should_get_reservations_name() throws Exception {
        String reservationLocation = getReservationLocation();
        mockMvc.perform(get(reservationLocation))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].username").value("Sofia"));
    }

    @Test
    void should_get_reservations() throws Exception {
        String reservationLocation = getReservationLocation();
        mockMvc.perform(get(reservationLocation))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].startTime.client.zoneId").value("Africa/Nairobi"));
    }

    private String getLocation() throws Exception {
        MvcResult mvcResult = saveStaff();
        return mvcResult.getResponse().getHeader("location");
    }

    private Long getSavedStaffId() throws Exception {
        String location = getLocation();
        String[] locationSplit = location.split("/");
        return Long.valueOf(locationSplit[locationSplit.length - 1]);
    }

    private MvcResult saveStaff() throws Exception {
        String sendRequestStaff = serialize(SAVED_STAFF);
        return mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendRequestStaff))
                .andReturn();
    }

    private String getReservationLocation() throws Exception {
        String requestClientReservation = serialize(new RequestClientReservation("Sofia",
                "ThoughtWorks", "Africa/Nairobi", ZonedDateTime.now(), "PT1H"));
        Long savedStaffId = getSavedStaffId();
        updateZoneId(savedStaffId, "Asia/Chongqing");
        return mockMvc.perform(post(String.format("/api/staffs/%d/reservations", savedStaffId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestClientReservation)).andReturn().getResponse().getHeader("location");
    }

    private void updateZoneId(Long staffId, String zoneId) throws Exception {
        mockMvc.perform(put(String.format("/api/staffs/%d/timezone", staffId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new RequestZoneId(zoneId))));
    }
}
