package com.twuc.webApp.domain;

import com.twuc.webApp.domain.entity.Staff;
import com.twuc.webApp.domain.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class EntityTest{

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_and_get_staff() {
        Staff staff = new Staff("Rob", "Hall");
        Staff savedStaff = staffRepository.save(staff);
        entityManager.flush();
        entityManager.clear();
        Optional<Staff> fetchedStaffOptional = staffRepository.findById(savedStaff.getId());
        assertTrue(fetchedStaffOptional.isPresent());
        assertEquals("Rob", fetchedStaffOptional.get().getFirstName());
        assertEquals("Hall", fetchedStaffOptional.get().getLastName());
    }
}