1. create staff entity and table
2. given a well-formed rob when i post `api/staffs` I should get 201
3. given a well-formed rob when i post `api/staffs` I should get a correct location header
4. given an Incorrect format rob when I post `api/staffs` I should get 400 
5. given an exist staff in database when I get `api/staffs/{id}` I should get staff message
6. given an not exist staff id when I get `api/staffs/{id}` I should get 404
7. given a database with staffs when I get `api/staffs` I should get all staffs 
8. given an empty staff table when I get `api/staffs` I should get empty array
9. update staff entity and refactoring current test where used staff constructor
10. update staff table in database
11. should return 400 when zone Id does not exist 
12. should update staff zoneId when i put `/api/staffs/{staffId}/timezone`
13. should get zoneId list
14. given a request client reservation when i post to `/api/staffs/{staffId}/reservations` i should get 201
15. create reservation entity and table
16. get reservation info from database
17. create reservation response contract
18. encapsulation reservation to response reservation
